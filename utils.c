/* 
   Simplified dependencies for SDF
   Copyright (C) 2013-2014 Michael S. Warren <mswarren@gmail.com>

   Distributed under the terms of the BSD 3-clause License.

   The full license is in the file COPYING.txt, distributed with this software.
*/

#define _POSIX_C_SOURCE 200809L	/* for fileno */
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "utils.h"

int Msg_level;

int Msg_on(int level)
{
    Msg_level = level;
    return level;
}

void *Malloc(size_t n)
{
    void *p = malloc(n);
    if (!p) Error("Malloc failed %s %d", __FILE__, __LINE__);
    return p;
}

void *Calloc(size_t n, size_t s)
{
    void *p = calloc(n, s);
    if (!p) Error("Calloc failed %s %d", __FILE__, __LINE__);
    return p;
}

void *Realloc(void *q, size_t n)
{
    void *p = realloc(q, n);
    if (!p) Error("Realloc failed %s %d", __FILE__, __LINE__);
    return p;
}

/* MPMY I/O abstraction */

#include <unistd.h>
#include <sys/types.h>
#include <errno.h>

#define argcheck(f)							\
    if (((f)->type == FTYPE_POSIX) && ((f)->file == NULL)) {		\
        errno = EINVAL;							\
        return -1;							\
    } else if (((f)->type == FTYPE_URL) && ((f)->url_file == NULL)) {	\
        errno = EINVAL;							\
        return -1;							\
    }    

/* MPI only allows 2GB buffer sizes, and MPI_Get_Count uses an int */
#define MAXIOSIZE (256*1024*1024)

MPMYFile *MPMY_Fopen(const char *path, int mpmy_flags)
{
    MPMYFile *fp;
    int iomode = MPMY_SINGL;
    char mode[8] = {[0] = 'r'};

    Msgf("Fopen %s\n", path);
    if (mpmy_flags & MPMY_RDONLY) mode[0] = 'r'; /* MPMY_RDONLY is 0 since O_RDONLY is (stupidly) 0 */
    if (mpmy_flags & MPMY_WRONLY) mode[0] = 'w';
    if (mpmy_flags & MPMY_APPEND) mode[0] = 'a';

    if (mpmy_flags & MPMY_MULTI) iomode = MPMY_MULTI;
    if (mpmy_flags & MPMY_SINGL) iomode = MPMY_SINGL;

    if (mpmy_flags & MPMY_INDEPENDENT) {
	iomode = MPMY_MULTI;
    }

    fp = Calloc(1, sizeof(MPMYFile));
    if (path[0] == '-' && path[1] == '\0') {
	fp->type = FTYPE_POSIX;
	fp->file = stdin;
    } else if (url_isurl(path)) {
	fp->type = FTYPE_URL;
	fp->url_file = url_fopen(path, mode);
    } else {
	fp->type = FTYPE_POSIX;
	fp->file = fopen(path, mode);
    }
    if (fp->file) {
	Msg("Fopen returns %p, iomode=%d, flags=0x%x\n", fp, iomode, mpmy_flags);
    } else {
	Msgf("Fopen fails, errno=%d\n", errno);
    }
    return fp;
}

size_t MPMY_Fread(void *buf, size_t reclen, size_t nrecs, MPMYFile *fp)
{
    Msg("Fread(ptr=%p, size=%ld, nitems=%ld, FILE=%p)\n",
	 buf, reclen, nrecs, fp);

    argcheck(fp);
    if (fp->type == FTYPE_POSIX) {
	off_t len = 0;
	off_t nread = 0;
	while (len < reclen*nrecs) {
	    nread = read(fileno(fp->file), (char *)buf+len, reclen*nrecs-len);
	    if (nread == -1) {
		Warning("MPMY_Fread: read(%p, %"PRId64") failed, errno=%d\n", 
			fp->file, reclen*nrecs-len, errno);
		return -1;
	    } else if (nread == 0) {
		/* Warning("MPMY_Fread: read(%p, %ld) got EOF\n", 
		   fp->file, reclen*nrecs-len); */
		break;
	} else {
		Msg("MPMY_Fread(%p): got %"PRId64"\n", fp->file, nread);
		len += nread;
	    }
	}
	/* if (len != reclen*nrecs) Error("MPMY_Fread: Wrong amount of data\n"); */
	return len/reclen;
    } else if (fp->type == FTYPE_URL) {
	return url_fread(buf, reclen, nrecs, fp->url_file);
    } else {
	errno = EINVAL;
	return -1;
    }
}

size_t MPMY_Fwrite(const void *ptr, size_t size, size_t nitems, MPMYFile *fp)
{
    argcheck(fp);
    if (fp->type == FTYPE_POSIX) {
	size_t left = size*nitems;
	size_t offset = 0;
	while (left > 0) {
	    size_t nwrite = (left < MAXIOSIZE) ? left : MAXIOSIZE;
	    Msgf("write %zd at %zd\n", nwrite, offset);
	    ssize_t ret = write(fileno(fp->file), ptr+offset, nwrite);
	    if (ret != nwrite) {
		Warning("write failed\n");
		return -1;
	    }
	    left -= nwrite;
	offset += nwrite;
	}
	return nitems;
    } else if (fp->type == FTYPE_URL) {
	/* need to implement this */
	return -1;
    } else {
	return -1;
    }
}

size_t MPMY_Fseekrd(MPMYFile *fp, off_t offset, int whence, void *buf, size_t reclen,
				  size_t nrecs)
{
    argcheck(fp);
    if (fp->type == FTYPE_POSIX) {
	int fd = fileno(fp->file);
	int doseek;
	
	if (whence == SEEK_CUR) {
	    doseek = (offset != 0);
	} else if (whence == SEEK_SET) {
	    /* don't worry about errors.  If ftell returns -1, */
	    /* doseek will be turned on, and the fseek below will */
	    /* (probably) fail */
	    doseek = (lseek(fd, 0L, SEEK_CUR) != offset);
	} else {
	    doseek = 1;
	}
	
	if (doseek) {
	    int real_whence;
	    if (whence == MPMY_SEEK_SET) {
		real_whence = SEEK_SET;
	    } else if (whence == MPMY_SEEK_CUR) {
		real_whence = SEEK_CUR;
	    } else if (whence == MPMY_SEEK_END) {
		real_whence = SEEK_END;
	    } else {
		Warning("Illegal value of whence (%d) in MPMY_Fseekrd\n", whence);
		return -1;
	    }
	    if (lseek(fd, offset, real_whence) == -1) {
		Warning("MPMY_Fseekrd: lseek(%d, %"PRId64", %d) failed, errno=%d\n",
			fd, offset, whence, errno);
		return -1;
	    }
	}
	
	off_t len = 0;
	off_t nread = 0;
	
	while (len < reclen*nrecs) {
	    nread = read(fd, (char *)buf+len, reclen*nrecs-len);
	    if (nread == -1) {
		Warning("MPMY_Fseekrd: read(%p, %"PRId64") failed, errno=%d\n", 
			fp->file, reclen*nrecs-len, errno);
		return -1;
	    } else if (nread == 0) {
		Warning("MPMY_Fseekrd: read(%p, %"PRId64") got EOF\n", 
		    fp->file, reclen*nrecs-len);
		return -1;
	    } else {
		Msg("MPMY_Fseekrd(%p): got %"PRId64"\n", fp, nread);
		len += nread;
	    }
	}
	if (len != reclen*nrecs) Error("MPMY_Fseekrd: Wrong amount of data\n");
	return nread/reclen;
    } else if (fp->type == FTYPE_URL) {
	if (url_fseeko(fp->url_file, offset, whence)) return -1;
	return url_fread(buf, reclen, nrecs, fp->url_file);
    } else {
	return -1;
    }
}

off_t MPMY_Fseek(MPMYFile *fp, off_t offset, int whence)
{
    argcheck(fp);
    int real_whence = 0;
    if (whence == MPMY_SEEK_SET) real_whence = SEEK_SET;
    if (whence == MPMY_SEEK_CUR) real_whence = SEEK_CUR;
    if (whence == MPMY_SEEK_END) real_whence = SEEK_END;

    off_t ret;
    if (fp->type == FTYPE_POSIX) {
	ret = lseek(fileno(fp->file), offset, real_whence);
    } else if (fp->type == FTYPE_URL) {
	ret = url_fseeko(fp->url_file, offset, real_whence);
    } else ret = -1;

    if (ret != -1) ret = 0;
    Msgf("Fseek returns %"PRId64"\n", ret);
    return ret;
}

int MPMY_Fclose(MPMYFile *fp)
{
    argcheck(fp);
    int ret;
    if (fp->type == FTYPE_POSIX) {
	ret = close(fileno(fp->file));
    } else if (fp->type == FTYPE_URL) {
	ret = url_fclose(fp->url_file);
    } else ret = -1;
    Msg("Close returns %d\n", ret);
    return ret;
}

off_t MPMY_Flen(MPMYFile *fp)
{
    argcheck(fp);
    off_t ret;
    if (fp->type == FTYPE_POSIX) {
	ret = lseek(fileno(fp->file), 0, SEEK_END);
    } else if (fp->type == FTYPE_URL) {
	ret = url_flen(fp->url_file);
    } else ret = -1;
    Msgf("Flen returns %"PRId64"\n", ret);
    return ret;
}
