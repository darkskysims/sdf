/* 
   Simplified utilities for SDF
   Copyright (C) 2013-2014 Michael S. Warren <mswarren@gmail.com>
   
   Distributed under the terms of the BSD 3-clause License.

   The full license is in the file COPYING.txt, distributed with this software.
*/
#ifndef UTILS_H
#define UTILS_H

/* Debugging macros */

#include <stdio.h>

extern int Msg_level;
int Msg_on(int);

#define Msg(...) if (Msg_level) fprintf(stderr, __VA_ARGS__)
#define Msgf(...) if (Msg_level) fprintf(stderr, __VA_ARGS__)
#define Msg_do(...) if (Msg_level) fprintf(stderr, __VA_ARGS__)

#define Error(...) { fprintf(stderr, __VA_ARGS__); exit(1); }
#define Warning(...) fprintf(stderr, __VA_ARGS__)
#define singlWarning(...) fprintf(stderr, __VA_ARGS__)

/* Malloc wrappers */

#include <stdlib.h>

#define Free(p) free(p)

void *Malloc(size_t n);
void *Calloc(size_t n, size_t s);
void *Realloc(void *q, size_t n);

/* MPMY I/O abstraction */

#include <sys/types.h>
#include "urlio.h"

enum MPMY_ftype {
    FTYPE_NONE = 0,
    FTYPE_POSIX = 1,
    FTYPE_URL = 2
};

typedef struct MPMYFile {
    enum MPMY_ftype type;
    URL_FILE *url_file;		/* would like anonymous union here */
    FILE *file;
} MPMYFile;

#define	MPMY_RDONLY 00000000
#define	MPMY_WRONLY 00000001
#define	MPMY_RDWR   00000002
#define	MPMY_APPEND 00000004
#define	MPMY_CREAT  00000010
#define	MPMY_TRUNC  00000020
/* io modes */
#define MPMY_SINGL  00010000	/* like cubix single mode */
#define MPMY_MULTI  00020000	/* like cubix multi mode  */

#define MPMY_UNIX   00040000	/* one file, UNIX multi-process semantics */
#define MPMY_IOZERO 00100000	/* if(procnum==0){...} */
#define MPMY_INDEPENDENT 00200000 /* many files.  Complete independence */
#define MPMY_NFILE 00400000	/* many files. Really. */

/* modes for seek */
#define MPMY_SEEK_SET 0
#define MPMY_SEEK_CUR 1
#define MPMY_SEEK_END 2

/* Disable these for this implementation */
#define MPMY_Initialized() 1
#define MPMY_Init(a, b) 

MPMYFile *MPMY_Fopen(const char *path, int mpmy_flags);
size_t MPMY_Fread(void *buf, size_t reclen, size_t nrecs, MPMYFile *fp);
size_t MPMY_Fwrite(const void *buf, size_t reclen, size_t nrecs, MPMYFile *fp);
size_t MPMY_Fseekrd(MPMYFile *fp, off_t offset, int whence, void *buf, size_t reclen,
		    size_t nrecs);
off_t MPMY_Fseek(MPMYFile *fp, off_t offset, int whence);
off_t MPMY_Flen(MPMYFile *fp);
int MPMY_Fclose(MPMYFile *fp);

#endif /* UTILS_H */
