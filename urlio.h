/*
  POSIX I/O interface to URLs
  Copyright (c) 2013-2014 Michael S. Warren <mswarren@gmail.com> 
  All rights reserved.

  Distributed under the terms of the BSD 3-clause License.
*/
#ifndef URLIO_H
#define URLIO_H

#include <stdlib.h>
#ifdef HAS_CURL
#include <curl/curl.h>
#endif

#define URLIO_MAGIC 0x3a1a

/* We'd like to leave the native FILE * alone as much as possible */
/* Distinguish URL_FILE from FILE using value of magic */
/* Probably safer to keep track of which pointers are URL_FILE pointers */
typedef struct URL_FILE {
    int magic;
#ifdef HAS_CURL
    CURL *curl;
#endif
    int status;
    int header_size;
    long filetime;
    double header_time;
    double header_download;
    off_t st_size;

    char *buf;			/* external buffer */
    off_t buf_sz;
    off_t buf_pos;
    off_t buf_offset;
    off_t fpos;			/* file pointer */

    char *url_buf;		/* internal buffer */
    off_t url_buf_sz;
    off_t url_buf_pos;
    off_t url_buf_offset;
    off_t upos;			/* read position in URL */
} URL_FILE;

#ifdef HAS_CURL
URL_FILE *url_fopen(const char *url, const char *mode);
int url_fclose(URL_FILE *stream);
size_t url_fread(void *buf, size_t reclen, size_t nrecs, URL_FILE *stream);
int url_fseek(URL_FILE *stream, long offset, int whence);
int url_fseeko(URL_FILE *stream, off_t offset, int whence);
long url_ftell(URL_FILE *stream);
off_t url_ftello(URL_FILE *stream);
void url_rewind(URL_FILE *stream);
off_t url_flen(URL_FILE *stream);
int url_isurl(const char *path);
int url_debug(int level);
#else
/* stubs so as not to pollute code with ifdefs */
#define url_fopen(a, b) NULL
#define url_fclose(a) 0
#define url_fread(a, b, c, d) 0
#define url_fseek(a, b, c) 0
#define url_fseeko(a, b, c) 0
#define url_ftell(a) 0
#define url_ftello(a) 0
#define url_rewind(a) 0
#define url_flen(a) 0
#define url_isurl(a) 0
#define url_debug(a) 0
#endif

#endif /* URLIO_H */
