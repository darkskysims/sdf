CC=gcc
CFLAGS=-g -O2 -Wall -I. -std=c99 -fPIC
DEPS = SDF.h utils.h byteswap.h
libSDFOBJ = SDF-parse.o SDFfuncs.o SDFget.o SDFhdrio.o byteswap.o obstack.o utils.o version.o
executables = SDFcvt txttoSDF
libraries = libSDF.a

HAS_CURL=$(shell if [ `pkg-config --exists libcurl` ]; then echo "1"; else echo "0"; fi)
ifeq ($(HAS_CURL), 1)
	CFLAGS += -DHAS_CURL $(shell pkg-config --cflags libcurl)
	libSDFOBJ += urlio.o
	extlibs += $(shell pkg-config --libs libcurl)
else ifneq ("$(wildcard /usr/lib/libcurl.*)","") 
	CFLAGS += -DHAS_CURL
	libSDFOBJ += urlio.o
	extlibs += -lcurl
endif

all: $(libraries) $(executables)

libSDF.a: $(libSDFOBJ)
	ar -rs $@ $^

SDFcvt: SDFcvt.o libSDF.a
	$(CC) -o $@ $< libSDF.a $(extlibs)

txttoSDF: txttoSDF.o
	$(CC) -o $@ $<

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

SDF-parse.c: SDF-parse.y SDF-lex.c
	yacc -o $@ $<

SDF-lex.c: SDF-lex.l
	flex -o $@ $<

# version.distro.c is created by our pre- and post-commit hooks,
# and should be included in any distributed tarballs.
#
# When a user builds SDF from a tarball, there is no
# .git/, so version.c is copied from version.distro.c
# When a user builds SDF in a bona fide git repo, 
# .version.c is re-created on every build and then
# moved to version.c if it differs.
.PHONY: .version.c
.version.c: 
	@if [ -d .git ]; then \
	    echo char libSDF_version\[\] = "\"`git describe --tags  --dirty `\";" > .version.c; \
	else\
	    cp version.distro.c .version.c ; \
	fi

version.c: .version.c
	@cmp -s .version.c version.c || ( set -x; mv .version.c version.c )

.PRECIOUS: SDF-parse.c SDF-lex.c

check: SDFcvt
	cd tests; ./runcheck

.PHONY: clean
clean:
	/bin/rm -f $(libraries) $(executables) *.o

.PHONY: distclean
distclean: clean
	/bin/rm -f SDF-parse.c SDF-lex.c version.c
