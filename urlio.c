/*
  POSIX I/O interface to URLs

  Copyright (c) 2013-2014 Michael S. Warren <mswarren@gmail.com> 

  All rights reserved.

  Distributed under the terms of the BSD 3-clause License.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:

  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
  Tried fopen.c in curl examples, but doesn't support seek.
  libfetch from BSD also tries a similar interface.
*/
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include "utils.h"
#include "urlio.h"

#define HEADER_LEN 4096
#define BUFSZ 32768 /* best value depends on network speed and cache behavior */

#define Max(a,b) \
    ({ __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

#define Min(a,b) \
    ({ __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b; })

static int url_verbose = 0;

int url_isurl(const char *path)
{
    int ret = 0;
    if (!strncasecmp(path, "http://",  7)) ret = 1;
    if (!strncasecmp(path, "https://", 8)) ret = 1;
    if (!strncasecmp(path, "ftp://",   6)) ret = 1;
    if (!strncasecmp(path, "file://",  7)) ret = 1;
    return ret;
}

int url_debug(int level)
{
    url_verbose = level;
    return 0;
}

int url_stream(FILE *stream)
{
    URL_FILE *s = (URL_FILE *)stream;
    if (s && s->magic == URLIO_MAGIC) return 1;
    else return 0;
}

static size_t recv_data(char *buffer, size_t size, size_t nitems, void *p)
{
    URL_FILE *u = p;

    size_t n = size * nitems;
    size_t n_ext = Min(n, u->buf_sz - u->buf_offset);
    u->upos += n;

    if (n_ext) {		/* deliver to external buffer */
	Msg("URL %ld bytes to external\n", n_ext);
	if (u->buf_offset + n_ext > u->buf_sz) Error("Buffer overflow in recv_data\n");
	memcpy(u->buf + u->buf_offset, buffer, n_ext);
	u->buf_offset += n_ext;
	u->fpos += n_ext;
	n -= n_ext;
    }

    if (n) {			/* deliver to internal buffer */
	Msg("URL %ld bytes to internal\n", n);
	/* Can be big for directory listings or servers that don't honor range */
	if (u->url_buf_pos + n > u->url_buf_sz) {
	    u->url_buf_sz *= 2;
	    if (u->url_buf_pos + n > u->url_buf_sz)
		u->url_buf_sz = u->url_buf_pos + n;
	    u->url_buf = realloc(u->url_buf, u->url_buf_sz);
	    if (!u->url_buf) Error("Out of memory in recv_data\n");
	    Msg("URL realloc internal to %jd\n", (intmax_t)u->url_buf_sz);
	}
	memcpy(u->url_buf + u->url_buf_pos, buffer + n_ext, n);
	u->url_buf_pos += n;
    }

    return size*nitems;
}

static size_t parse_header_line(char *buffer, size_t size, size_t nitems, void *p)
{
    URL_FILE *u = p;

    /* Msg("URL HEADER %s", buffer); */
    if (!strncmp(buffer, "Content-Range: bytes ", strlen("Content-Range: bytes "))) {
	long int len;
	sscanf(buffer, "Content-Range: bytes %*d-%*d/%ld\n", &len);
	u->st_size = len;		/* type conversion */
    } else if ((u->st_size == 0) &&	/* Don't overwrite full file size from Range */
	       !strncmp(buffer, "Content-Length: ", strlen("Content-Length: "))) {
	long int len;
	sscanf(buffer, "Content-Length: %ld\n", &len);
	u->st_size = len;		/* type conversion */
    }
    return size*nitems;
}

size_t url_fread(void *buf, size_t reclen, size_t nrecs, URL_FILE *u)
{
    if (u == NULL) {
	errno = EINVAL;
	return -1;
    }
    if (u->fpos == u->st_size) return 0; /* EOF */
    off_t start_pos = u->fpos;
    off_t end_pos = Min(u->fpos + reclen * nrecs, u->st_size);
    u->buf = buf;
    u->buf_pos = u->buf_offset = 0;
    u->buf_sz = Min(reclen * nrecs, end_pos - start_pos);

    size_t n = Min(u->buf_sz, u->url_buf_pos - u->url_buf_offset);
    if (n) {
	memcpy(buf, u->url_buf + u->url_buf_offset, n);
	u->url_buf_offset += n;
	u->buf_offset += n;
	u->fpos += n;
    }

    /* fill buf up to buf_sz, remainder goes in url_buf */
    /* Keep chunks aligned to BUFZ, to make it possible?/easier to cache range requests */
    if (u->upos < end_pos) {
	char range[64];
	if (end_pos != u->st_size && end_pos % BUFSZ) {
	    end_pos += BUFSZ - (end_pos % BUFSZ);
	}
	u->url_buf_pos = u->url_buf_offset = 0;
	snprintf(range, sizeof(range), "%lld-%lld", (long long int)u->upos, (long long int)end_pos-1);
	curl_easy_setopt(u->curl, CURLOPT_RANGE, range);
	Msg("Read URL %s\n", range);
	CURLcode res = curl_easy_perform(u->curl);
	if (res != CURLE_OK) {
	    Msg("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	}
    }
    return (u->fpos-start_pos)/reclen;
}

int url_fseek(URL_FILE *stream, long offset, int whence)
{
    if (stream == NULL) {
	errno = EINVAL;
	return -1;
    }
    if (whence == SEEK_SET) {
	stream->fpos = offset;
    } else if (whence == SEEK_CUR) {
	stream->fpos += offset;
    } else if (whence == SEEK_END) {
	stream->fpos = stream->st_size + offset;
    }
    stream->upos = stream->fpos;
    stream->url_buf_pos = stream->url_buf_offset = 0; /* flush internal buffer */
    return 0;
}

int url_fseeko(URL_FILE *stream, off_t offset, int whence)
{
    if (stream == NULL) {
	errno = EINVAL;
	return -1;
    }
    if (whence == SEEK_SET) {
	stream->fpos = offset;
    } else if (whence == SEEK_CUR) {
	stream->fpos += offset;
    } else if (whence == SEEK_END) {
	stream->fpos = stream->st_size + offset;
    }
    stream->upos = stream->fpos;
    stream->url_buf_pos = stream->url_buf_offset = 0; /* flush internal buffer */
    return 0;
}

long url_ftell(URL_FILE *stream)
{
    if (stream == NULL) {
	errno = EINVAL;
	return -1;
    }
    if (stream->fpos > LONG_MAX) {
	errno = EOVERFLOW;
	return -1;
    }
    return stream->fpos;
}

off_t url_ftello(URL_FILE *stream)
{
    if (stream == NULL) {
	errno = EINVAL;
	return -1;
    }
    return stream->fpos;
}

void url_rewind(URL_FILE *stream)
{
    if (stream == NULL) {
	errno = EINVAL;
	return;
    }
    stream->fpos = 0;
}

int url_fclose(URL_FILE *stream)
{
    if (stream == NULL) {
	errno = EINVAL;
	return -1;
    }
    curl_easy_cleanup(stream->curl);
    free(stream->url_buf);
    free(stream);
    return 0;
}

off_t url_flen(URL_FILE *stream)
{
    if (stream == NULL) {
	errno = EINVAL;
	return -1;
    }
    return stream->st_size;
}

URL_FILE *url_fopen(const char *url, const char *mode)
{
    URL_FILE *u;

    if (!(u = calloc(1, sizeof(URL_FILE)))) {
	errno = ENOMEM;
	return NULL;
    }
    u->magic = URLIO_MAGIC;
    u->url_buf = malloc(BUFSZ);
    u->url_buf_sz = BUFSZ;
    if (!u->url_buf) {
	errno = ENOMEM;
	return 0;
    }
    if (!(u->curl = curl_easy_init())) {
	/* set errno here */
	return NULL;
    }
    curl_easy_setopt(u->curl, CURLOPT_URL, url);
    curl_easy_setopt(u->curl, CURLOPT_VERBOSE, url_verbose);
    curl_easy_setopt(u->curl, CURLOPT_HEADERDATA, u);
    curl_easy_setopt(u->curl, CURLOPT_HEADERFUNCTION, parse_header_line);
    curl_easy_setopt(u->curl, CURLOPT_FAILONERROR, 1);
    curl_easy_setopt(u->curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(u->curl, CURLOPT_WRITEDATA, u);
    curl_easy_setopt(u->curl, CURLOPT_WRITEFUNCTION, recv_data);
    curl_easy_setopt(u->curl, CURLOPT_FILETIME, 1);
    curl_easy_setopt(u->curl, CURLOPT_USERAGENT, "SDFcvt/1.0");
    char range[32];
    snprintf(range, sizeof(range), "0-%lld", (long long int)u->url_buf_sz-1);
    curl_easy_setopt(u->curl, CURLOPT_RANGE, range);
    Msg("Open URL %s\n", range);
    CURLcode res = curl_easy_perform(u->curl);
    if (res != CURLE_OK) {
	Msg("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	url_fclose(u);
	return NULL;
    }

    double curlinfo_st_size;
    curl_easy_getinfo(u->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &curlinfo_st_size);
    Msg("curlinfo_size %.0lf st_size %jd\n", curlinfo_st_size, (intmax_t)u->st_size);
    if (curlinfo_st_size > u->st_size) u->st_size = curlinfo_st_size;

    curl_easy_getinfo(u->curl, CURLINFO_FILETIME, &u->filetime);
    if (u->filetime == -1) {
	/* apache doesn't give last-modified for directories */
	u->filetime = time(NULL); /* now */
    }
    curl_easy_getinfo(u->curl, CURLINFO_TOTAL_TIME, &u->header_time);
    curl_easy_getinfo(u->curl, CURLINFO_SIZE_DOWNLOAD, &u->header_download);

    return u;
}

